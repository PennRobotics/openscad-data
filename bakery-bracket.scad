$fn=60;
EPS=0.01;

h=25.4;
t_base=3.175;
d_bot=9.525;  // diameter at bottom of mount surface
d_top=9.525;  // diameter at top of mount surface
d_end=6.25;
l_end=19.05;
t_end=3.175;
dist_peg=12.7;
h_peg=3.175;
d_peg=3.175;
taper_peg=0.175;  // how much narrower the end is
nudge=-1.5875;

hull() {
  translate([0,0,d_bot/2]) rotate([90,0,90]) cylinder(h=EPS, d=d_bot);
  translate([0,0,h-d_top/2]) rotate([90,0,90]) cylinder(h=EPS, d=d_top);
  translate([t_base/2,0,d_bot/2]) scale([t_base/d_bot,1,1]) sphere(d=d_bot);
  translate([t_base/2,0,h-d_top/2]) scale([t_base/d_bot,1,1]) sphere(d=d_top);
  translate([l_end-d_end/2,+t_end/8,h-d_end/2]) scale([1,t_end/(1.25*d_end),1]) sphere(d=d_end);
  translate([l_end-d_end/2,-t_end/8,h-d_end/2]) scale([1,t_end/(1.25*d_end),1]) sphere(d=d_end);
}
translate([0,0,(h-dist_peg)/2+nudge]) rotate([-90,0,90]) cylinder(h=h_peg, d1=d_peg, d2=d_peg-taper_peg);
translate([0,0,(h+dist_peg)/2+nudge]) rotate([-90,0,90]) cylinder(h=h_peg, d1=d_peg, d2=d_peg-taper_peg);