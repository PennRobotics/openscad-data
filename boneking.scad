//*
rangetest=0;
/*/
//rangetest=265;
rangetest=75;
// */

//*
packtest=0;
a=12.3;
/*/
packtest=220;
a=90;
translate([-110,-110,0]) rotate([90,0,45]) text("Collapsed height:");
translate([-110,-110,-15]) rotate([90,0,45]) text("682 mm");
translate([0,0,-32]) cylinder(h=0.1, d=150, center=true);
// */

mute_type=0;

/*
COL_CAST="#48b098";
COL_LEGS="#30a078";
COL_TOP="#f08060";
COL_BOTTOM="#6080f0";
COL_CLAMP="#38393a";
COL_CONE="#c0a050";
COL_MARKER="#00ff0010";
/*/
COL_CAST="#58585b";
COL_LEGS="#6c645a";
COL_TOP="#6c645a";
COL_BOTTOM="#6c645a";
COL_CLAMP="#38393a";
COL_CONE="#505050";
COL_MARKER="#00ff0010";
// */

/************************************************/
LH=343;
SSD=40;
T=-38;
H_BOTTOM=330;
H_TOP=545;
H_SLIM=220;
$fn=60;

translate([-60, -60, 870]) rotate([90,0,45]) text("No top threads (inner nor outer)");
translate([-135, -135, 855]) rotate([90,0,45]) text("Hollow tube preferred: any wall thickness, tapped later if needed");

translate([30,30,240]) rotate([90,0,45]) text("Lower tube shortened", 11, ":style=Bold");
translate([50,50,225]) rotate([90,0,45]) text("to 330 mm", 11, ":style=Bold");
translate([26,26,420]) rotate([90,0,45]) text("(Same tube clamps as 1100)");

translate([-175,-175,690]) rotate([90,0,45]) text("Narrow section: 220 mm", 11, ":style=Bold");
translate([ -91, -91,675]) rotate([90,0,45]) text("15.9±0.1 OD", 11, ":style=Bold");

translate([-180,-180,480]) rotate([90,0,45]) text("Upper tube: overall 545 mm", 11, ":style=Bold");
translate([-200,-200,465]) rotate([90,0,45]) text("(thicker section 20.7 OD; same as 1100)");

translate([37, 37, 910])  rotate([90,0,45]) text("← aprx. max height of tip from floor: 915 mm");
translate([37, 37, 645])  rotate([90,0,45]) text("← aprx. min height of tip from floor: 650 mm");

translate([-110,-110,120]) rotate([90,0,45]) text("MicKing 1100");
translate([-107,-107,105]) rotate([90,0,45]) text("Tripod Base");

difference() {
  union() {
    // Base tripod
    translate([0, 0, packtest]) difference() {
      translate([0, 0, 91]) union() {
        color(COL_LEGS) rotate([0, 0, 000]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
        color(COL_LEGS) rotate([0, 0, 120]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
        color(COL_LEGS) rotate([0, 0, 240]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
        color(COL_CAST) rotate([0, 0, 000]) translate([0,T, 0]) rotate([90+a,0,0]) translate([0,0,LH-34]) cylinder(h = 34, d=36);
        color(COL_CAST) rotate([0, 0, 120]) translate([0,T, 0]) rotate([90+a,0,0]) translate([0,0,LH-34]) cylinder(h = 34, d=36);
        color(COL_CAST) rotate([0, 0, 240]) translate([0,T, 0]) rotate([90+a,0,0]) translate([0,0,LH-34]) cylinder(h = 34, d=36);
        color(COL_CAST) hull() {
          rotate([0, 0, 000]) translate([0,T, 0]) sphere(d=SSD);
          rotate([0, 0, 000]) translate([0,T*.55, 0]) sphere(d=SSD/2);
        }
        color(COL_CAST) hull() {
          rotate([0, 0, 120]) translate([0,T, 0]) sphere(d=SSD);
          rotate([0, 0, 120]) translate([0,T*.55, 0]) sphere(d=SSD/2);
        }
        color(COL_CAST) hull() {
          rotate([0, 0, 240]) translate([0,T, 0]) sphere(d=SSD);
          rotate([0, 0, 240]) translate([0,T*.55, 0]) sphere(d=SSD/2);
        }
        color(COL_CAST) hull() {
          rotate([0, 0, 000]) translate([0,T*.7, 0]) sphere(d=SSD/2);
          cylinder(h=20, d=28);
        }
        color(COL_CAST) hull() {
          rotate([0, 0, 120]) translate([0,T*.7, 0]) sphere(d=SSD/2);
          cylinder(h=20, d=28);
        }
        color(COL_CAST) hull() {
          rotate([0, 0, 240]) translate([0,T*.7, 0]) sphere(d=SSD/2);
          cylinder(h=20, d=28);
        }
        color(COL_CAST) translate([0,0,-7]) cylinder(h=37, d=28);
        color(COL_CLAMP) translate([0,0,28]) union() {
          cylinder(h=35, d=33);
          translate([12, 0, 1]) cube([10,10,33]);
          translate([0, 0, 20]) rotate([0,0,45]) translate([21,0,0]) cylinder(h=12, d=7);
          translate([0, 0, 20]) for (i=[0:5:120]) {
            hull() {
              rotate([0,0,i+45]) translate([19.5,0,0]) cube([5-0.025*i, 0.1, 12]);
              rotate([0,0,i+50]) translate([19.5,0,0]) cube([5-0.025*i, 0.1, 12]);
            }
          }
        }
      }
      cylinder(h=200, d=25.5);
    }

    // Bottom tube
    difference() {
      union() {
        translate([0, 0, 83]) color(COL_BOTTOM) cylinder(h=H_BOTTOM, d=25.4);
        translate([0, 0, 83+H_BOTTOM-35+3.5]) color(COL_CLAMP) union() {
          cylinder(h=35, d=29);
          translate([10,0,1]) cube([10,10,33]);
          translate([0, 0, 20]) rotate([0,0,45]) translate([19,0,0]) cylinder(h=12, d=6);
          translate([0, 0, 20]) for (i=[0:5:120]) {
            hull() {
              rotate([0,0,i+45]) translate([18,0,0]) cube([4-0.02*i, 0.1, 12]);
              rotate([0,0,i+50]) translate([18,0,0]) cube([4-0.02*i, 0.1, 12]);
            }
          }
        }
      }
      cylinder(h=2000, d=22.7);
    }
  }

  // CUTAWAY
  translate([-4, -22, 80+packtest]) cube([18, 18, 1000]);
  //translate([-13, 0, 80]) cube([26, 100, 1000]);
}

// Top tube
translate([0, 0, rangetest+105]) color(COL_TOP) union() {
  cylinder(h=H_TOP, d=15.8);
  hull() {
    cylinder(h=H_TOP-H_SLIM, d=15.8);
    cylinder(h=H_TOP-H_SLIM-8, d=20.7);
    translate([0, 0, H_TOP-H_SLIM-8]) sphere(d=20.7);
  }
  cylinder(h=8, d=22.7);
}

//*
// Ideal cone height, K&M tenor
    if (mute_type==1)
translate([0, 0, max(570, 500+rangetest)]) color(COL_CONE) union() {
  translate([0, 0, min(130, 60+rangetest)]) cylinder(h=30, d=25);
  difference() {
    scale([1, 1, 0.5]) sphere(d=80);
    translate([0,0,-15]) cube([100, 100, 30], center=true);
  }
  translate([0, 0, -30]) cylinder(h=35,d=25);
}
// */

//*
// Ideal cone height, K&M bass
    if (mute_type==2)
translate([0, 0, max(520, 465+rangetest)]) color(COL_CONE) union() {
      if (rangetest >= 20)
  translate([0, 0, min(175, 120+rangetest)]) sphere(d=35);
  difference() {
    union() {
      hull() {
        translate([0, 0,  0]) scale([1, 1, 0.35]) sphere(d=140);
        translate([0, 0, 16]) scale([1, 1, 0.2]) sphere(d=112);
      }
      hull() {
        translate([0, 0, 16]) scale([1, 1, 0.2]) sphere(d=112);
        translate([0, 0, 28]) scale([1, 1, 0.2]) sphere(d=88);
      }
      hull() {
        translate([0, 0, 28]) scale([1, 1, 0.2]) sphere(d=88);
        translate([0, 0, 50]) scale([1, 1, 0.2]) sphere(d=60);
      }
      hull() {
        translate([0, 0, 50]) scale([1, 1, 0.2]) sphere(d=60);
        translate([0, 0, 64]) scale([1, 1, 0.2]) sphere(d=52);
      }
      hull() {
        translate([0, 0, 64]) scale([1, 1, 0.2]) sphere(d=52);
        translate([0, 0,120]) scale([1, 1, 0.15]) sphere(d=43);
      }
    }
    translate([0,0,-20]) cube([150, 150, 40], center=true);
  }
  translate([0, 0, -30]) cylinder(h=35,d=25);
}
// */

//*
// Ideal cone height, Kolberg
    if (mute_type==3)
translate([0, 0, max(500, 476+rangetest)]) union() {
      if (rangetest >= 14)
  color("#fcfcfc") translate([0, 0, min(189, 165+rangetest)]) sphere(d=40, $fn=120);
  color("#7c7176") {
    hull() {
      translate([0, 0,  0]) cylinder(h=15, d=160);
      translate([0, 0, 26]) cylinder(d=110);
    }
    hull() {
      translate([0, 0, 26]) cylinder(d=110);
      translate([0, 0, 40]) cylinder(d=78);
    }
    hull() {
      translate([0, 0, 40]) cylinder(d=78);
      translate([0, 0, 55]) cylinder(d=62);
    }
    hull() {
      translate([0, 0, 55]) cylinder(d=62);
      translate([0, 0, 90]) cylinder(d=50);
    }
    hull() {
      translate([0, 0, 90]) cylinder(d=50);
      translate([0, 0,159]) cylinder(d=42);
    }
  }
  color("#dbdbde") translate([0, 0, -45]) cylinder(h=50,d=19);
}
// */

// Height spec
color(COL_MARKER) hull() {
  translate([0,0,650]) cylinder(h=1, d=50, center=true);
  translate([0,0,915]) cylinder(h=1, d=50, center=true);
}

// DISTANCE
// * to bell flare: 510
// * to stand tip: 750 (29mm bumper, 35mm trombone outer wall)