LH=343;
a=12.3;
SSD=40;
T=-38;

// Base tripod
difference() {
union() {
difference() {
 translate([0,0,91]) union() {
  rotate([0, 0, 000]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
  rotate([0, 0, 120]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
  rotate([0, 0, 240]) translate([0,T, 0]) rotate([90+a,0,0]) cylinder(h = LH, d=25.4);
  hull() {
    rotate([0, 0, 000]) translate([0,T, 0]) sphere(d=SSD);
    rotate([0, 0, 000]) translate([0,T*.55, 0]) sphere(d=SSD/2);
  }
  hull() {
    rotate([0, 0, 120]) translate([0,T, 0]) sphere(d=SSD);
    rotate([0, 0, 120]) translate([0,T*.55, 0]) sphere(d=SSD/2);
  }
  hull() {
    rotate([0, 0, 240]) translate([0,T, 0]) sphere(d=SSD);
    rotate([0, 0, 240]) translate([0,T*.55, 0]) sphere(d=SSD/2);
  }
  hull() {
    rotate([0, 0, 000]) translate([0,T*.7, 0]) sphere(d=SSD/2);
    cylinder(h=20, d=28);
  }
  hull() {
    rotate([0, 0, 120]) translate([0,T*.7, 0]) sphere(d=SSD/2);
    cylinder(h=20, d=28);
  }
  hull() {
    rotate([0, 0, 240]) translate([0,T*.7, 0]) sphere(d=SSD/2);
    cylinder(h=20, d=28);
  }
  translate([0,0,-7]) cylinder(h=37, d=28);
  translate([0,0,28]) cylinder(h=35, d=33);
 }
 cylinder(h=150, d=25.5);
}

// Bottom tube
difference() {
  union() {
    translate([0,0,83]) cylinder(h=742, d=25.4);
    translate([0,0,83+742-35]) cylinder(h=35, d=29);
  }
  cylinder(h=1000, d=22.7);
}
}

translate([-13,0,80]) cube([26, 100, 1000]);
}

// Top tube
translate([0,0,660+105]) union() {
  cylinder(h=806, d=15.8);
  hull() {
    cylinder(h=806-34, d=15.8);
    cylinder(h=806-34-8, d=20.7);
    translate([0,0,806-34-8]) sphere(d=20.7);
  }
  cylinder(h=8, d=22.7);
}

// Official height spec
translate([0,0,950]) sphere(3);
translate([0,0,1635]) sphere(3);