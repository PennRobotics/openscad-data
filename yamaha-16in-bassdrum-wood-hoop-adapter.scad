TH=2.6;
IN=TH+1;
EPS=.01;

$fn=120;

translate([0.1,0,TH/2]) difference() {
  union() {
    hull() {
      translate([10.4,0,0])  cylinder(h=TH, d=21.2, center=true);
      translate([1, 9.9,0])  cylinder(h=TH, r=1, center=true);
      translate([1,-9.9,0])  cylinder(h=TH, r=1, center=true);
      translate([4, 10,0])   cylinder(h=TH, r=1.05, center=true);
      translate([4,-10,0])   cylinder(h=TH, r=1.05, center=true);
      translate([9.6, 10,0]) cylinder(h=TH, r=1.16, center=true);
      translate([9.6,-10,0]) cylinder(h=TH, r=1.16, center=true);
      translate([15,0,0])    cylinder(h=TH, d=22.2, center=true);
      translate([20,0,0])    cylinder(h=TH, d=21.5, center=true);
      translate([25,0,0])    cylinder(h=TH, d=20.2, center=true);
      translate([30,0,0])    cylinder(h=TH, d=18.2, center=true);
      translate([38, 6.1,0]) cylinder(h=TH, r=1.1, center=true);
      translate([38,-6.1,0]) cylinder(h=TH, r=1.1, center=true);
      translate([38,0,0])    cylinder(h=TH, d=13.3, center=true);
    }
    /*
    translate([0,0,0.5]) hull() {
      translate([2.5, 8.8,0]) cylinder(h=IN, r=1, center=true);
      translate([2.5,-8.8,0]) cylinder(h=IN, r=1, center=true);
      translate([4, 8.7,0]) cylinder(h=IN, r=1, center=true);
      translate([4,-8.7,0]) cylinder(h=IN, r=1, center=true);
      translate([15,0,0]) cylinder(h=IN, d=19, center=true);
      translate([20,0,0]) cylinder(h=IN, d=18.2, center=true);
      translate([25,0,0]) cylinder(h=IN, d=17, center=true);
      translate([30,0,0]) cylinder(h=IN, d=15, center=true);
      translate([38,0,0]) cylinder(h=IN, d=10, center=true);
    }
    // */
  }
  translate([7,0,0]) cylinder(h=TH+EPS, d=7.1, center=true, $fn=60);
  translate([38,0,0]) cylinder(h=TH+EPS, d=7.1, center=true, $fn=60);

  translate([-1,0,0.99+8*25.4]) rotate([0,90,0]) cylinder(h=50, d=16*25.4, $fn=1080);

  rotate([0,0,  0]) translate([-2,-10+11.0,-11.20]) rotate([25,0,0]) cube([50,20,5]);
  rotate([0,0, -3]) translate([-2,-10+11.5,-11.35]) rotate([25,0,0]) cube([50,20,5]);
  rotate([0,0, -6]) translate([-2,-10+12.2,-11.55]) rotate([25,0,0]) cube([50,20,5]);
  rotate([0,0,-10]) translate([-2,-10+14.0,-11.50]) rotate([25,0,0]) cube([50,20,5]);
  rotate([0,0,-16]) translate([-2,-10+18.0,-11.10]) rotate([25,0,0]) cube([50,20,5]);

  rotate([0,0, 0]) translate([-2,-10-11.0,-1.90]) rotate([-25,0,0]) cube([50,20,5]);
  rotate([0,0, 3]) translate([-2,-10-11.5,-2.05]) rotate([-25,0,0]) cube([50,20,5]);
  rotate([0,0, 6]) translate([-2,-10-12.2,-2.25]) rotate([-25,0,0]) cube([50,20,5]);
  rotate([0,0,10]) translate([-2,-10-14.0,-2.20]) rotate([-25,0,0]) cube([50,20,5]);
  rotate([0,0,16]) translate([-2,-10-18.0,-1.80]) rotate([-25,0,0]) cube([50,20,5]);

  translate([-10,0,-2.4]) rotate([2,25,0]) cube([10,25,5]);
  translate([-10,-12,-1.9]) rotate([-2,25,0]) cube([10,25,5]);
}