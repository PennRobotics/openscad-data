K=1.200;  // flare constant

H=70;
D1=28;
D2=100;
th=1.3;  // bell thickness

Wcut=20;  // width of cross section

// Overhang support
Hsup=20;
D2sup=59;
D1sup=78;

Krnd=0.205;  // rounding constant

// Precision
$fn=180;
st=120;
eps=0.01;


use <PrintBone/bessel.scad>;

module do_bell(solid) {
  rotate([180, 0, 0]) rotate_extrude() bessel_curve(throat_radius=D1/2, mouth_radius=D2/2, length=H, flare=K, wall_thickness=th, solid=solid, steps=st);
}

intersection() {

  union() {
    // Middle panel
    intersection() {
      do_bell(solid=true);
      //translate([0, 0, H/2]) cube([D2, 2.2, H], center=true);  // Flat
      rotate([0, -90, 0]) linear_extrude(D2, center=true) polygon(points=[[0, -1.3], [H, -0.3], [H, 0.3], [0, 1.3]]);  // Tapered
    }

    // Overhang support (near print bed)
    intersection() {
      do_bell(solid=true);
      difference() {
        translate([0, 0, Hsup/2]) cube([D2, 20, Hsup], center=true);
        translate([0, 0, -eps]) cylinder(Hsup+1, d1=D1sup, d2=D2sup);
      }
    }

    // Main bell section
    intersection() {
      do_bell(solid=false);
      translate([0, 0, H/2]) cube([D2, Wcut, H], center=true);
    }
  }

  // Round edges
  scale([1, Krnd, 1]) cylinder(h=H+1, d=D2+0.1);
}
