/**
 *  Latch Lake Replacement Bearing / Clutch Bracket
 *
 *  This is NOT an official part!
 *
 *  Use a strong, non-brittle material:
 *   - Nylon
 *   - ASA
 *   - PC
 *   - PP
 */

H=5; // TODO: measure
Hmin=2; // TODO: measure
W=14; // TODO: measure
L=18; // TODO: measure
Dlatch=14; // TODO: measure
Dhole=5.4; // TODO: measure
EPS=.01;

$fn=60;

difference() {
  translate([0,0,H/2]) cube([W,L,H], center=true);
  translate([0,0,Dlatch/2+Hmin]) rotate([90,0,0]) cylinder(h=L+EPS, d=Dlatch, center=true);
  cylinder(h=2*H+EPS, d=Dhole, center=true);
}
