W=11.00;
W2=9.70;
wc=0.70;

D=15.00;
Da=6.40;
Db=7.90;
dc=1.20;

Ht=1.00;
H=0.70;

Lt=1.84;  // Dist at center to thumb bulge at its base
R=44.3153;
k1=1.6;  // Determines angle of bulge

r=0.8;
rs=0.24;

rot = 28;  // degrees at tip

// Dimension check:
/// cube([W,D,Ht], center=true);

ds = D/2 + rs;
ws = W2 - W/2 + rs;
xy = W - W2;
wcut = ws + xy + 1;
points = [
      [ws, ds],
      [ws, xy + ds - Da],
      [ws + xy, ds - Da],
      [ws + xy, ds - Db - 2*rs],
      [ws + xy - wc, ds - Db - 2*rs],
      [ws + xy - wc, ds - Db - dc],
      [ws + xy, -wc + ds - Db - dc],
      [wcut, -wc + ds - Db - dc],
      [wcut, ds],
    ];

difference() {

  union() {
    // Thumb grip
    intersection() {
      minkowski() {
        cylinder(Ht/2, r, r, center=true, $fn=30);
        cube([W - 2*r, D - 2*r, Ht/2], center=true);
      }
      translate([0, -R - D/2 + Lt, 0])
          cylinder(Ht, R + H*k1, R + (H-Ht)*k1,
                   center=true, $fn=180);
    }

    // Rounded full length of card, tip-width
    minkowski() {
      cylinder(H/2, r, r, center=true, $fn=30);
      translate([(W2-W)/2, 0, (H-Ht)/2])
          cube([W2 - 2*r, D - 2*r, H/2], center=true);
    }

    // Full width of card; needs subtraction!
    translate([0, -r, (H-Ht)/2])
        cube([W, D-4*r, H], center=true);
  }

  // Edge cutout
  minkowski() {
    cylinder(Ht/2, rs, rs, center=true, $fn=24);
    linear_extrude(Ht, center=true) polygon(points);
  }

  // Slope at tip
  translate([0, D/2, -Ht + H/2])
      rotate([rot, 0, 0])
      cube([W, D, Ht], center=true);
}