/* Nothing has been measured, just the general shape replicated */
/* For personal use only! */

H=10;
$fn=60;
rotate([90,0,0]) difference() {
  union() {
    hull() {
      translate([0, 0, 0]) cylinder(h=H, d=10, center=true);
      translate([-8, 0, 0]) cylinder(h=H, d=6, center=true);
    }
    hull() {
      translate([0, -0.5, 0]) cylinder(h=H, d=8, center=true);
      translate([-9, 1, 0]) cylinder(h=H, d=8.8, center=true);
    }
    hull() {
      translate([-9, -1, 0]) cylinder(h=H, d=7, center=true);
      translate([-14, 6, 0]) cylinder(h=H, d=5, center=true);
      translate([-2, 0, 0]) cylinder(h=H, d=5, center=true);
    }
  }
  minkowski() {
    difference() {
      translate([-14, 6, 3.4]) cylinder(h=H, d=14, center=true);
      translate([-14, 6, 3.4]) cylinder(h=H, d=9, center=true);
    }
    sphere(r=0.4, $fn=24);
  }
  minkowski() {
    translate([0, 0, 0]) cylinder(h=H-2*2.8, d=10, center=true);
    sphere(r=0.4, $fn=24);
  }
  translate([0, 0, 6]) cylinder(h=4, d=6.8, center=true, $fn=6);
  translate([0, 0, 0]) cylinder(h=2*H, d=4, center=true);
  translate([0, 0,-6]) cylinder(h=4, d=6.2, center=true);
  hull() {
    translate([-12, 1, 0]) sphere(d=3.6);
    translate([-18, 0, 0]) sphere(d=4.8);
  }
  hull() {
    translate([-9, 3, 0]) sphere(d=3.6);
    translate([-6, 10, 0]) sphere(d=4.8);
  }
}