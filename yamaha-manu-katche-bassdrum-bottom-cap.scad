/* Plate */
Wp=100;
Lp=80;
Rp=10;
Hp=9.1;
Tmin=2.8;
Cutoff=1.0;  /* for attaching a metal plate underneath, metal thickness */
//_DEBUG=7.2;/*TODO-debug*/

/* Holes (drill) */
Wh=80;  /* UNUSED */
Lh=60;
Dh=9.0;
A_h=11.1363;  /* deg */
K=5.4;  /* max extra length */
SDh=6.4;  /* shank diameter */
HDh=12;  /* head diameter of screws */
HHh=4.9;  /* head height of screws */
BDh=12.8;  /* bore diameter */

/* Foot hole filler */
Df1=22.6;
Df2=24.0;
Hf=9.5;
Lrear=24.8;

/* Bass drum profile */
Db=16*25.4;

EPS=0.01;
$fn=60;

difference() {

union() {
  difference() {
    /* Plate */
    hull() {
      translate([ Wp/2-Rp, Lp/2-Rp,0]) cylinder(h=Hp, r=Rp);
      translate([-Wp/2+Rp,-Lp/2+Rp,0]) cylinder(h=Hp, r=Rp);
      translate([ Wp/2-Rp,-Lp/2+Rp,0]) cylinder(h=Hp, r=Rp);
      translate([-Wp/2+Rp, Lp/2-Rp,0]) cylinder(h=Hp, r=Rp);
    }
    
    /* Bass drum diameter */
    translate([0,0,Db/2+Tmin]) rotate([90,0,0]) cylinder(h=2*Lp, d=Db, center=true, $fn=360);

    /* Drill holes */    
    for (X=[-1:2:1.1]) {
      for (Y=[-1:2:1.1]) {
        translate([0,Y*Lh/2,Db/2]) rotate([0,X*A_h,0]) union () {
          translate([0,0,-Db/2-K]) cylinder(h=20, d=Dh);
          translate([0,0,-Db/2-K]) cylinder(h=K, d=BDh);
          translate([0,0,-Db/2-EPS]) cylinder(h=HHh, d1=HDh, d2=SDh);
        }
      }
    }
  }

  /* Foot hole filler */
  hull() {
    translate([0,Lp/2-Df1/2-Lrear,Tmin-EPS]) cylinder(h=EPS, d=Df2);
    translate([0,Lp/2-Df1/2-Lrear,Tmin+Hf-1]) cylinder(h=EPS, d=Df1);
    translate([0,Lp/2-Df1/2-Lrear,Tmin+Hf]) cylinder(h=EPS, d=Df1-1);
  }
}

translate([-Wp,-Lp,-2*EPS]) cube([2*Wp, 2*Lp, Cutoff]);
//translate([-Wp,-Lp,_DEBUG]) cube([2*Wp, 2*Lp, 100]);/*TODO-debug*/
//translate([50,0,-2*EPS]) scale([1.2,1,1]) cylinder(h=100, d=48);/*TODO-debug*/
//translate([-50,0,-2*EPS]) scale([1.2,1,1]) cylinder(h=100, d=48);/*TODO-debug*/
//translate([0,54,-2*EPS]) cylinder(h=100, d=64);/*TODO-debug*/
//translate([0,-54,-2*EPS]) scale([1,1.25,1]) cylinder(h=100, d=64);/*TODO-debug*/
}