// Fills gap between Manhasset 2700 music stand weight and its M10 bolt

$fn=180;
difference() {
  hull() {
    cylinder(h=15.4, d=26.5);
    translate([0,0,2.7]) cylinder(h=10, d=27);
  }
  translate([0,0,-.3]) cylinder(h=16, d1=10.3, d2=10);
  translate([0,0,-.3]) cylinder(h=16, d1=10, d2=10.3);
}