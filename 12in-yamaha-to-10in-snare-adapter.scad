EPS=0.01;
$fn=60;

difference() {
  translate([-13,-14,-3]) cube([26,54,35]);  /* Main block */
  hull() {  /* Inner track for inserting a rubber gripper */
    translate([0,27.5,4.2]) rotate([0,90,0]) cylinder(h=20, r=2.5, center=true);
    translate([0,27.5,45.]) rotate([0,90,0]) cylinder(h=20, r=2.5, center=true);
    translate([0,57.5,4.2]) rotate([0,90,0]) cylinder(h=20, r=2.5, center=true);
    translate([0,57.5,45.]) rotate([0,90,0]) cylinder(h=20, r=2.5, center=true);
  }
  hull() {  /* Outer wall of inserted rubber gripper */
    translate([0,28.0,4.7]) rotate([0,90,0]) cylinder(h=27, r=2.1, center=true);
    translate([0,28.0,45.]) rotate([0,90,0]) cylinder(h=27, r=2.1, center=true);
    translate([0,57.5,4.7]) rotate([0,90,0]) cylinder(h=27, r=2.1, center=true);
    translate([0,57.5,45.]) rotate([0,90,0]) cylinder(h=27, r=2.1, center=true);
  }
  translate([-22.6/2,-10.5-EPS,-3-EPS]) cube([22.6,34.5,5.2]);  /* Space below for existing gripper */
  translate([-16.8/2,0,-3-EPS]) cube([16.8,50,3]);
  difference() {
    hull() {
      translate([-11,-10.4,2]) cube([22,10.4,EPS]);
      translate([-10.6,-10.4,32+EPS]) cube([21.2,9.9,EPS]);
    }
    hull() {
      translate([-3.5,-0.8,2]) cube([7,2,EPS]);
      translate([-3.5,-1.3,32+EPS]) cube([7,2,EPS]);
    }
  }
  hull() {
    translate([0,32,45]) rotate([0,90,0]) cylinder(h=30, d=3, center=true);
    translate([0, 5,20]) rotate([0,90,0]) cylinder(h=30, d=3, center=true);
    translate([0, 5,48]) rotate([0,90,0]) cylinder(h=30, d=3, center=true);
  }
  difference() {
    translate([0,23,36.44]) cube([30,15,15], center=true);
    translate([0,22.22,23]) rotate([0,90,0]) cylinder(h=31, d=16, center=true);
  }
}

